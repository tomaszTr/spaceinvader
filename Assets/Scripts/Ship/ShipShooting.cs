﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WeponsDescription //////////////// maybe i shoud ScritableObject
{
    public int dmg = 1;
    public int idWeapon;
    public Transform shotTransform;
    public BuletScript prefabShotinObject;
    public float fireTime = 0.05f;
    [HideInInspector()]
    public float lastTimeFire = 0;
    public bool haveWeapon;
    public bool mustHaveAmmo=false;
    public int ammoCount;
}
public class ShipShooting : MonoBehaviour
{
    public WeponsDescription[] tab;
    private List<WeponsDescription> listWeponsDescription;
    private bool player;
    void Awake()
    {
        if (this.gameObject.tag == "Player")
            player = true;
        else
            player = false;
        listWeponsDescription = new List<WeponsDescription>();
        for (int i = 0; i < tab.Length; i++)
        {
            listWeponsDescription.Add(tab[i]);
        }
    }
    public void AddWeaponDescription(int id)
    {
        for (int i = 0; i < listWeponsDescription.Count; i++)
        {
            if (listWeponsDescription[i].idWeapon == id)
            {
                listWeponsDescription[i].haveWeapon = true;
            }
        }
    }
    
    public int Ammo
    {
        get
        {
            if (listWeponsDescription.Count < 2)
                return 0;
           return listWeponsDescription[1].ammoCount;
        }
    }
    public void AddAmmoToWeapon(int idWepon,int count)
    {
        foreach (var weapodescription in listWeponsDescription)
        {
            if(weapodescription.idWeapon==idWepon)
            weapodescription.ammoCount += count;
        }
    }
    [HideInInspector]
    public bool disarm = false;
    public void shootAll()
    {
        if (disarm)
            return;
        for (int i = 0; i < listWeponsDescription.Count; i++)
        {
            myShoot(listWeponsDescription[i]);
        }
    }
    public void shoot(int idWeapon)
    {
        for (int i = 0; i < listWeponsDescription.Count; i++)
        {
            if(listWeponsDescription[i].idWeapon == idWeapon)
            {
                myShoot(listWeponsDescription[i]);
            }
        }
    }
    private void myShoot(WeponsDescription weaponDescription)
    {
        if (Time.time - weaponDescription.lastTimeFire > weaponDescription.fireTime)
        {
            if (!weaponDescription.mustHaveAmmo || weaponDescription.haveWeapon)//////////////HAVE WEAPON
            {
                if (!weaponDescription.mustHaveAmmo || weaponDescription.ammoCount > 0)////////////////JEŚLI JEST AMUNICJA
                {
                    weaponDescription.ammoCount--;
                    weaponDescription.lastTimeFire = Time.time;
                    GameObject go = ObjectPoolScript.current.GetPooledObject(weaponDescription.prefabShotinObject.gameObject);
                   // if (go == null)
                   //     return;
                    BuletScript obj = go.GetComponent<BuletScript>();
                   // if (obj == null)
                   //     return;
                    obj.transform.position = weaponDescription.shotTransform.position;
                    obj.transform.rotation = weaponDescription.shotTransform.rotation;
                    //obj.vectorOfShoting = Vector2.up;// new Vector2(weaponDescription.shotTransform.up.x, weaponDescription.shotTransform.up.y);
                    obj.dmg = weaponDescription.dmg;
                    if(player)
                        obj.gameObject.layer = LayerMask.NameToLayer("Player");
                    else
                        obj.gameObject.layer = LayerMask.NameToLayer("Enemy");
                    obj.gameObject.SetActive(true);
                    

                }
            }
        }
        
    }
}
