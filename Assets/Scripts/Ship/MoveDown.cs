﻿using UnityEngine;
using System.Collections;

public class MoveDown : MonoBehaviour
{

    public float speed=5;
    void FixedUpdate()
    {
        transform.Translate(0,-speed * Time.fixedDeltaTime, 0);
    }
}
