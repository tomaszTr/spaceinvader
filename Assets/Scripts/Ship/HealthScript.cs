﻿using UnityEngine;
using System.Collections;
using System;

public class HealthScript : MonoBehaviour
{

    public ParticleSystem prefabExplosion;
    void Awake()
    {
        health = maxHealth;
    }
    
    public int maxHealth = 1;
    private int health;
    public int Health
    {
        get
        {
            return health;
        }
    }
    public void Heal()
    {
        health = maxHealth;
    }

    public int scoreOnDestroy = 10;
    [HideInInspector()]
    public bool immunityOnDmg=false;
    public event Action Deth;
    public void GetDmg(int dmg)
    {
        if (immunityOnDmg)
            return;
        if (dmg < 0)
            dmg = 0;
        GameObject go = ObjectPoolScript.current.GetPooledObject(prefabExplosion.gameObject);
        go.transform.position = this.transform.position;
        go.SetActive(true);
        health -= dmg;
        if (health <= 0)
        {
            if(Deth!=null)
                Deth();
            if (this.gameObject.tag != "Player")
            {
                GameObject.FindObjectOfType<Player>().AddScore(scoreOnDestroy);
                Destroy(this.gameObject);
            }
        }
    }
}
