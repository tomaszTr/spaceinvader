﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(HealthScript))]
public class ShipMoving : MonoBehaviour
{

    public const int dmgOnColision = 10;
    public Vector2 velocity
    {
        get
        {
            return rigiBody.velocity;
        }
    }
    [SerializeField]
    private Vector2 maxSpead = new Vector2(5, 5);
    public Vector2 MaxSpead
    {
        get
        {
            return maxSpead;
        }
    }
    
    private Rigidbody2D rigiBody;
    private HealthScript healthScript;

    public void moveLeft()
    {
        rigiBody.velocity = new Vector2(-maxSpead.x, rigiBody.velocity.y);
    }
    public void moveRight()
    {
        rigiBody.velocity = new Vector2(maxSpead.x, rigiBody.velocity.y);
    }
    public void moveUp()
    {
        rigiBody.velocity = new Vector2(rigiBody.velocity.x, -maxSpead.y);
    }
    public void moveDown()
    {
        rigiBody.velocity = new Vector2(rigiBody.velocity.x, maxSpead.y);
    }

    void Awake()
    {
        rigiBody = this.GetComponent<Rigidbody2D>();
        healthScript = this.GetComponent<HealthScript>();
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<HealthScript>() != null)
        {
            collision.gameObject.GetComponent<HealthScript>().GetDmg(dmgOnColision);
            healthScript.GetDmg(dmgOnColision);
        }
    }
}
