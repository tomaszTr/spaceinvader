﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{
    public System.Action bossDeth;

    public WeaponGet weaponGet;
    public float timeWhenSpawnWeaponGet;
    public AmmoGet ammoGet;
    public float[] timeAmmoSpawn;
    public AIBose bose;
    public float timeWhenBoos=30f;
    public AIMoveShot[] tab;
    public float timeNextShip = 1;
    public Transform xMinStart;
    public Transform xMaxStart;

    private float startTime;
    private bool boseCreate = false;
    private float difficulty = 1;
    private float lastNextShipTime;

    void Start()
    {
        startTime = Time.time;
    }
    void Update()
    {
        if (Time.time - startTime > timeWhenSpawnWeaponGet)
        {
            timeWhenSpawnWeaponGet = float.MaxValue;/////////////Spawn one time weapon
            Instantiate(weaponGet, new Vector3(Random.Range(xMinStart.position.x, xMaxStart.position.x)
                , xMinStart.position.y, 0), weaponGet.transform.rotation);
        }
        for (int i = 0; i < timeAmmoSpawn.Length; i++)
        {
            if (Time.time - startTime > timeAmmoSpawn[i])
            {
                timeAmmoSpawn[i] = float.MaxValue;
                Instantiate(ammoGet, new Vector3(Random.Range(xMinStart.position.x, xMaxStart.position.x)
                , xMinStart.position.y, 0), ammoGet.transform.rotation);
            }
        }
        if (boseCreate)
            return;
        if (Time.time - startTime > timeWhenBoos)
        {
            GameObject toCreate = bose.gameObject;
            GameObject boseGo = (GameObject)Instantiate(toCreate, new Vector3(Random.Range(xMinStart.position.x, xMaxStart.position.x)
                , xMinStart.position.y, 0), toCreate.transform.rotation);
            boseGo.GetComponent<HealthScript>().Deth += Boss_Deth;
            boseCreate = true;
        }
        else if (Time.time - startTime - lastNextShipTime > timeNextShip * difficulty)
        {
            difficulty *= 0.99f;
            GameObject toCreate = tab[Random.Range(0, tab.Length)].gameObject;
            Instantiate(toCreate, new Vector3(Random.Range(xMinStart.position.x,xMaxStart.position.x)
                ,xMinStart.position.y,0), toCreate.transform.rotation);
            lastNextShipTime = Time.time - startTime;
        }
    }

    private void Boss_Deth()
    {
        if (bossDeth != null)
            bossDeth();
    }
}
