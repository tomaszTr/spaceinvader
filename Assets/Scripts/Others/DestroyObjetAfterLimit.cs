﻿using UnityEngine;
using System.Collections;

public class DestroyObjetAfterLimit : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BotomDestroyColider")
        {
            Destroy(this.gameObject);
        }
    }
}
