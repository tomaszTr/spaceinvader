﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameObjectAndPooledAmount
{
    public int pooledAmount;
    public bool willGrow;
    public GameObject pooleObject;
    [HideInInspector]
    public List<GameObject> pooledObjects;
}
public class ObjectPoolScript : MonoBehaviour  
{
    public static ObjectPoolScript current;
    [SerializeField]
    public GameObjectAndPooledAmount[] gameObjectAndPooledAmount;

    public void Awake()
    {
        current = this;
    }
    void Start()
    {
        for (int i = 0; i < gameObjectAndPooledAmount.Length; i++)
        {
            gameObjectAndPooledAmount[i].pooledObjects = new List<GameObject>();
            for (int j = 0; j < gameObjectAndPooledAmount[i].pooledAmount; j++)
            {
                GameObject obj = (GameObject)Instantiate(gameObjectAndPooledAmount[i].pooleObject);
                obj.SetActive(false);
                gameObjectAndPooledAmount[i].pooledObjects.Add(obj);
                obj.transform.SetParent(this.gameObject.transform);
            }
        }
    }

    public GameObject GetPooledObject(GameObject prefab)
    {
        for (int i = 0; i < gameObjectAndPooledAmount.Length; i++)
        {
            if(gameObjectAndPooledAmount[i].pooleObject == prefab)
            {
                for (int j = 0; j < gameObjectAndPooledAmount[i].pooledObjects.Count; j++)
                {
                    if (!gameObjectAndPooledAmount[i].pooledObjects[j].activeInHierarchy)
                    {
                        return gameObjectAndPooledAmount[i].pooledObjects[j];
                    }
                }
                if (gameObjectAndPooledAmount[i].willGrow)
                {
                    GameObject obj = (GameObject)Instantiate(gameObjectAndPooledAmount[i].pooleObject);
                    gameObjectAndPooledAmount[i].pooledObjects.Add(obj);
                    obj.transform.SetParent(this.gameObject.transform);
                    return obj;
                }
                return null;
            }
            
        }
        return null;
    }
}
