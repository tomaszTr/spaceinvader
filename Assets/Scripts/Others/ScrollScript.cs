﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
public class ScrollScript : MonoBehaviour
{
    public float speed = 0.1f;
    private MeshRenderer meshRenderer;
    void Start()
    {
        meshRenderer = this.GetComponent<MeshRenderer>();
    }
    void Update()
    {
        meshRenderer.material.mainTextureOffset = new Vector2(0, Time.time * speed);
    }
}
