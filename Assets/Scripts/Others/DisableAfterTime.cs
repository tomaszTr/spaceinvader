﻿using UnityEngine;
using System.Collections;
public class DisableAfterTime : MonoBehaviour
{
    public float disableAfterTime = 3f;
    void OnEnable()
    {
        Invoke("MyDestroy", disableAfterTime);
    }
    void OnDisable()
    {
        CancelInvoke();
    }
    void MyDestroy()
    {
        gameObject.SetActive(false);
    }
}
