﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class DisplayPlayer : MonoBehaviour
{

    public bool basicWersionGame = false;
    public Text ammoCount;
    public Image health;

    public Player player;
    public Text score;
    public Image prefabImageLife;
    public GameObject laoutGameObject;
    public Image endGame;
    public Text GameEnd;
    public EnemySpawner enemySpawner;
    private List<GameObject> imgesLife;
    private int maxLife=10;
    private int count;

    void Start()
    {
        Time.timeScale = 1;
        for (int i = laoutGameObject.transform.childCount; i < maxLife; i++)
        {
            Instantiate(prefabImageLife, laoutGameObject.transform);
        }
        imgesLife = new List<GameObject>();
        for (int i = 0; i < laoutGameObject.transform.childCount; i++)
        {
            imgesLife.Add(laoutGameObject.transform.GetChild(i).gameObject);
        }
        player.EndGameLost += Player_EndGameLost;
        endGame.gameObject.SetActive(false);
        enemySpawner.bossDeth += EndGameWean;
    }
    public void EndGameWean()
    {
        Time.timeScale = 0;
        GameEnd.text = "You Wean";
        endGame.gameObject.SetActive(true);
    }
    private void Player_EndGameLost()
    {
        Time.timeScale = 0;
        GameEnd.text = "You Lost";
        endGame.gameObject.SetActive(true);
    }
    public void RestartGameButtonClick()
    {
        SceneManager.LoadScene(0);
    }
    void Update()
    {
        score.text = player.score.ToString()+"Points";
        if (!basicWersionGame)
        {
            ammoCount.text = player.Ammo + " Special ammo";
            health.fillAmount = player.HpZeroToOne;
        }
        count = 0;
        foreach (var item in imgesLife)
        { 
            if(count<player.life)
                item.SetActive(true);
            else
                item.SetActive(false);
            count++;
        }
    }
}
