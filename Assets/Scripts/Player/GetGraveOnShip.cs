﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Player))]
[RequireComponent(typeof(HealthScript))]
[RequireComponent(typeof(ShipShooting))]
public class GetGraveOnShip : MonoBehaviour
{
    public float timeOfGrave = 3;
    public SpriteRenderer spriteRenderer;

    private float startGrave=float.MinValue;
    private HealthScript healhScript;
    private ShipShooting ShipShooting;
    private bool graveOn=false;
    private float timeStart;

    public void GetGrave()
    {
        graveOn = true;
        healhScript.immunityOnDmg = true;
        startGrave = Time.time - timeStart;
        this.gameObject.layer = LayerMask.NameToLayer("PlayerImmune");
        ShipShooting.disarm = true;
    }


    void Start()
    {
        healhScript = this.gameObject.GetComponent<HealthScript>();
        ShipShooting = this.gameObject.GetComponent<ShipShooting>();
        timeStart = Time.time;
    }
    void Update()
    {
        if (!graveOn)
            return;
        if (Time.time - timeStart - startGrave < timeOfGrave)
        {
            spriteRenderer.color = new Color(Color.white.r, Color.white.g, Color.white.b, Mathf.Lerp(1,0,Mathf.Repeat(Time.time- timeStart, 1)));
        }
        else
        {
            ShipShooting.disarm = false;
            healhScript.immunityOnDmg = false;
            spriteRenderer.color = Color.white;
            this.gameObject.layer = LayerMask.NameToLayer("Player");
            graveOn = false;
        }
    }
}
