﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(GetGraveOnShip))]
[RequireComponent(typeof(HealthScript))]
[RequireComponent(typeof(ShipShooting))]
public class Player : MonoBehaviour
{
    public event Action EndGameLost;
    
    public int life = 3;
    public void AddScore(int add)
    {
        score += add;
    }
    public int Ammo
    {
        get
        {
            return shipShooting.Ammo;
        }
    }
    public float HpZeroToOne
    {
        get
        {
            return healhScript.Health / (float)healhScript.maxHealth;
        }
    }
    [HideInInspector]
    public int score = 0;
    
    private HealthScript healhScript;
    private GetGraveOnShip getGraveOnShip;
    private ShipShooting shipShooting;

    void Start()
    {
        getGraveOnShip = this.GetComponent<GetGraveOnShip>();
        healhScript = this.GetComponent<HealthScript>();
        shipShooting = this.GetComponent<ShipShooting>();
        healhScript.Deth += HealhScript_Deth;
    }
    private void HealhScript_Deth()
    {
        getGraveOnShip.GetGrave();
        life--;
        healhScript.Heal();
        if (life <= 0)
        {
            if (EndGameLost != null)
                EndGameLost();
        }
    }
    
}
