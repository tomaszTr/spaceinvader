﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class AmmoGet : MonoBehaviour {

    public int idWeapon = 0;
    public int count = 20;
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && collision.gameObject.GetComponent<ShipShooting>() != null)
        {
            collision.gameObject.GetComponent<ShipShooting>().AddAmmoToWeapon(idWeapon,count);
            Destroy(this.gameObject);
        }
    }
}
