﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class WeaponGet : MonoBehaviour
{
    public int idWeapon=0;
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && collision.gameObject.GetComponent<ShipShooting>()!=null)
        {
            collision.gameObject.GetComponent<ShipShooting>().AddWeaponDescription(idWeapon);
            Destroy(this.gameObject);
        }
    }
}
