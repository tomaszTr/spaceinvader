﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

class KeyCodeTabAndAction
{
    public KeyCode[] tab;
    public Action action;
    public KeyCodeTabAndAction(KeyCode[] tab,Action action)
    {
        this.tab = tab;
        this.action = action;
    }
}

[RequireComponent(typeof(ShipMoving))]
[RequireComponent(typeof(ShipShooting))]
public class PlayerInput : MonoBehaviour
{

    public KeyCode[] moveRight = new KeyCode[] { KeyCode.RightArrow };
    public KeyCode[] moveLeft = new KeyCode[] { KeyCode.LeftArrow };
    public KeyCode[] moveUp = new KeyCode[] { KeyCode.UpArrow};
    public KeyCode[] moveDown = new KeyCode[] { KeyCode.DownArrow };
    public KeyCode[] shootWeapon = new KeyCode[] { KeyCode.Space };


    private ShipMoving moveShip;
    private ShipShooting shipShooting;
    private List<KeyCodeTabAndAction> tab;
    void Awake()
    {
        moveShip = this.GetComponent<ShipMoving>();
        shipShooting = this.GetComponent<ShipShooting>();
        tab = new List<KeyCodeTabAndAction>();
        tab.Add(new KeyCodeTabAndAction(moveRight,new Action(moveShip.moveRight)));
        tab.Add(new KeyCodeTabAndAction(moveLeft, new Action(moveShip.moveLeft)));
        tab.Add(new KeyCodeTabAndAction(moveUp, new Action(moveShip.moveDown)));
        tab.Add(new KeyCodeTabAndAction(moveDown, new Action(moveShip.moveUp)));
        tab.Add(new KeyCodeTabAndAction(shootWeapon, new Action(shipShooting.shootAll)));
    }

	void Update ()
    {
        foreach (KeyCodeTabAndAction item in tab)
        {
            for (int i = 0; i < item.tab.Length; i++)
            {
                if (Input.GetKey(item.tab[i]))
                    item.action();
            }
        }
    }


}
