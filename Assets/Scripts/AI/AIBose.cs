﻿using UnityEngine;
using System.Collections;

public class AIBose : MonoBehaviour
{
    public HealthScript cover;
    public Transform IsCoverTransform;
    public Transform IsNotCoverTransform;
    public float timeToMoveCover = 0.1f;

    public float timeCover=1f;
    public float timeIsNotCover = 2f;

    private float timeStart;
    private bool isCover=true;
    private float lastTimeChangeIsCover=0;
    void Start()
    {
        timeStart = Time.time;
    }
    void Update ()
    {
        if (isCover)
        {
            if (Time.time - timeStart - lastTimeChangeIsCover > timeIsNotCover)
            {
                isCover = false;
                lastTimeChangeIsCover = Time.time - timeStart;
            }
        }
        else
        {
            if (Time.time - timeStart - lastTimeChangeIsCover > timeCover)
            {
                isCover = true;
                lastTimeChangeIsCover = Time.time - timeStart;
            }
        }
        if (isCover)
            cover.transform.position = Vector3.Lerp(cover.transform.position, IsNotCoverTransform.position, (Time.time - timeStart  - lastTimeChangeIsCover) / timeToMoveCover);
        else
            cover.transform.position = Vector3.Lerp(cover.transform.position, IsCoverTransform.position, (Time.time - timeStart - lastTimeChangeIsCover) / timeToMoveCover);
    }

}
