﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ShipMoving))]
[RequireComponent(typeof(ShipShooting))]
public class AIMoveShot : MonoBehaviour
{
    private ShipMoving shipMowing;
    private ShipShooting shipShooting;
    private Player player;
    private const float EpsilionStopMove = 0.5f;
    private Vector3 startPosition;
    private float StartTime;

    public bool moveByTime=false;
    public float scaleMoveByTime = 1;
    public bool moveToEnemy = false;
    public bool moveForward = true;
    public bool moveDownUpByTime=false;
    public bool managerShut=false;
    void Awake()
    {
        shipMowing = this.GetComponent<ShipMoving>();
        shipShooting = this.GetComponent<ShipShooting>();
        player = GameObject.FindObjectOfType<Player>();
        startPosition = this.transform.position;
        StartTime = Time.time;
    }    
    void Update()
    {
        if(!managerShut)
            shipShooting.shootAll();

        if (moveToEnemy)
        {
            if (player != null)
            {
                if (this.transform.position.x - player.transform.position.x > EpsilionStopMove)
                    shipMowing.moveLeft();
                if (player.transform.position.x - this.transform.position.x > EpsilionStopMove)
                    shipMowing.moveRight();
            }
        }
        else if (moveByTime)
        {
          

                if ((1 - Mathf.Repeat((Time.time - StartTime) * scaleMoveByTime, 2)) < 0)
                    shipMowing.moveLeft();
                else
                    shipMowing.moveRight();
                if (moveDownUpByTime && (int)(Time.time - StartTime) % 4 ==0)
                {
                        shipMowing.moveDown();
                }
                if (moveDownUpByTime && (int)(Time.time % 4 - StartTime) == 1)
                {
                        shipMowing.moveUp();
                }
                if (moveDownUpByTime && Time.time - this.StartTime < 1)
                {
                    shipMowing.moveUp();
                }
        }
        if(moveForward)
            shipMowing.moveUp();
    }
}
