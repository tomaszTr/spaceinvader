﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveDown))]
[RequireComponent(typeof(Collider2D))]
public class BuletScript : MonoBehaviour
{
    public float destroyTimeAfterShut = 3f;
    public int dmg;

    void OnEnable()
    {
        Invoke("MyDestroy", destroyTimeAfterShut);
    }
    void OnDisable()
    {
        CancelInvoke();
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.GetComponent<HealthScript>().GetDmg(dmg);
        CancelInvoke();
        MyDestroy();
    }
    void MyDestroy()
    {
        gameObject.SetActive(false);
    }
}
